// Simple NOT gate module

module main( Switch, LED );
	input wire [5:0] Switch;
	output wire [7:0] LED;
	assign LED[7:0]=Switch[5:0];
endmodule
